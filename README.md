# Agnostic Boilerplate

![Codename: SILVER SNAKE](https://img.shields.io/badge/codename-silver_snake-blue.svg?longCache=true&style=for-the-badge&colorB=AEAEAE)
![Version: alpha²](https://img.shields.io/badge/version-alpha²-lightgrey.svg?longCache=true&style=for-the-badge&colorB=AEAEAE)

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/518cddde81834865ae9b76ceb9d40c0c)](https://www.codacy.com/app/jmurowaniecki/base) [![StyleCI](https://gitlab.styleci.io/repos/3861215/shield?branch=master)](https://gitlab.styleci.io/repos/3861215)

Este projeto foi criado com a finalidade de possibilitar organização, padronização e qualidade em padrões de desenvolvimento e fluxo de entrega desde o momento de sua criação visando boas práticas.

> Utilize como padrão para iniciar seus projetos de forma rápida e descomplicada com **Composer** e **Docker**.

---

## Estrutura

Modularize seus projetos em diretórios separados dentro de **./containers** (exemplo _./container/**nome_do_seu_projeto**_) de forma a facilitar o gerenciamento e organização de questões pertinentes ao código (não esquecendo que o container deve ser referenciado no arquivo **docker-compose.yml** para que seja devidamente construído e configurado da mesma forma que seu acesso a partir do proxy em **./containers/.services/balancer**).

Utilize as receitas criadas para gerar rapidamente seus projetos utilizando os templates de estruturas existentes com o comando `make new`.

### Arquivo .editorconfig
O arquivo `.editorconfig` foi criado para facilitar a coesão do time de desenvolvimento em utilizar os mesmos padrões para os tipos de arquivos que venham integrar o projeto.

#### Padrões a serem mantidos e respeitados
Para os diversos tipos de arquivos que venham a compor seu projeto, o time pode optar por utilizar espaçamento (soft tabs) ao invés de tabulação (hard tabs). Havendo uma convenção do padrão a ser utilizado, todos deverão adota-lo, por exemplo:
-   Codificação de caracteres: **UTF-8**;
-   Estilo/tipo de indentação: **espaços**;
-   Quantidade/tamanho indentação: **4**;

#### Utilize editores / IDEs capazes de interpretar este arquivo
A lista de editores e interfaces de desenvolvimento com recursos nativos ou suporte a plugins é extensa, porém podemos citar alguns que nosso time recomenda:
-   VI/VIM;
-   Atom;
-   Sublime Text;
-   PHPStorm;
-   Visual Studio;
-   Eclipse;
-   gEdit;


### Primeiros passos

Por padrão em sistemas Unix/Linux, o `Makefile` é um arquivo interpretado pelo comando `make`, que serve como padrão para a instalação de projetos nas plataformas suportadas.

Utilize o comando `make` para executar os comandos de instalação, montagem e configuração dos ambientes de forma simplificada.

#### Instalação e configuração

Execute o comando `make install`.

#### Diretórios/arquivos

##### ./.λ/\*

##### ./containers/\*
Arquivos de configuração dos containers criados pelo **docker-compose.yml**, mensagem de abertura e configuração dos serviços.

Projeto(s) em execução, balanceadores e serviços de manutenção.

##### .editorconfig
Padrões para arquivos do projeto.

##### .gitattributes

##### .gitignore

##### composer.json
Identificador do projeto - **MODIFICAR APÓS INSTALAÇÃO**.

##### docker-compose.yml
Estrutura dos Docker containers.

##### LICENSE.md

##### Makefile
Arquivo de montagem e manutenção do projeto.

##### phpcs.xml
Coleção de regras para assegurar PHP Coding Standart.

## Guia de Instalação

Execute o comando `composer -sdev create-project compilou/base nome_do_seuprojeto` para criar um projeto base na pasta _nome\_do\_seuprojeto_.

> Lembrando que a opção `-sdev` deve-se apenas para permitir a criação do projeto com a versão mais recente em desenvolvimento.

Após adquirir o projeto, execute o comando `make install` dentro da pasta do projeto para executar as rotinas de instalação e configuração, que resultará em uma saída conforme exemplo abaixo:

```text
λ@compilou:~$ composer create-project compilou/base projeto-teste
λ@compilou:~$ cd projeto-teste
λ@compilou:~/projeto-teste$ make install

docker-compose build

...
... Relatório extenso do build do Docker Compose.
...
... ATENÇÃO:
...          Esse processo pode demorar um pouco.
...

Removing intermediate container e79886545c15
Successfully built 30bbf113f2d5
Successfully tagged basephp_php7:latest
mariadbdata uses an image, skipping
mariadb uses an image, skipping

λ@compilou:~/projeto-teste$
```

> **Dica:** O comando `make install` executa o build dos containers e, logo a seguir, instala as dependências via `composer` e `npm`. Para mais informações sobre as opções de montagem e automação execute o comando `make` sem parâmetros.

## Inicialização

Utilize o comando `make start` para inicializar o projeto e `make stop` para finalizar os containers. A execução do comando `make start` exibirá uma saída similar ao exemplo abaixo:

```text
λ@compilou:~/projeto-teste$ make start

docker-compose up -d
Creating network "basephp_pipeline" with the default driver
Creating basephp_mariadbdata_1
Creating basephp_php7_1
Creating basephp_mariadb_1

λ@compilou:~/projeto-teste$
```
**Pronto!** Seu projeto já está disponível e pode ser acessado em seu [localhost](http://localhost/).

> **Dica:** se você tiver outros containers utilizando as portas padrão do Apache/NGinx/HTTPd e MariaDB/MySQL os serviços não serão inicializados. Para corrigir isso será necessário alterar essas portas para outras que estejam livres.

## Colabore

Envie sua sugestão abrindo uma issue, comentando ou realizando um pull request.

## License and terms of use

The author is not liable for misuse and/or damage caused by free use and/or distribution of this tool.

### <div style="text-align:center">The MIT License (MIT)</div>

#### <div style="text-align:center">Copyright © 2017 λ::lambda, CompilouIT, John Murowaniecki.</div>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions: The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
