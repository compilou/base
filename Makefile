LIBRARY = λ
SERVICE = bash $(LIBRARY) make
OPTIONS = `arg="$(filter-out $@,$(MAKECMDGOALS))" && echo $${arg:-${1}}`

.DEFAULT: help

help: $(LIBRARY)
	@$(SERVICE)

start:
	@$(SERVICE) start

debug:
	@$(SERVICE) debug

stop:
	@$(SERVICE) stop

install:
	@$(SERVICE) install

new:
	@$(SERVICE) new $(call OPTIONS,default)

%:
	@:
