# Roadmap

## Versão 1
-   [x] Criar coleção de *receitas de bolo* para os mais diversos propósitos e alimentar o automatizador com seus respectivos fluxos (no intuito de facilitar integrações);
-   [x] Autocomplete dos comandos (tanto do make quanto do automatizador);
-   [ ] Manual de uso das ferramentas;
-   [x] Automatizador deve carregar os módulos instalados e listar suas funcionalidades;
-   [x] Automatizador deve ser instalado como dependência, preferivelmente via composer;
-   [x] Comando `make`, executar scripts via `composer` de dependências que realizarão os processos de instalação, configuração e manutenção do ambiente;
-   [ ] Criar imagens previamente configuradas das imagens utilizadas;
-   [ ] Automatizar criação de chaves privadas;

## Backlog
-   [ ] Automatização dos processos de criação de projeto e referenciação automática com repositório;
