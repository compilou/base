#!/usr/bin/env bash
#
# TITLE       : λ::lambda Recipes
# DESCRIPTION : Receitas de gerenciamento simples dos comandos de manutenção do projeto.
# AUTHOR      : John Murowaniecki <john@compilou.com.br>
# DATE        : 20181006
# VERSION     : 0.3
# USAGE       : make
# USAGE       : bash λ
# USAGE       : ./λ
# USAGE       : ./λ help
# ----------------------------------------------------------------------------
#
_d=docker-compose

function environment {
    # Specific environment procedures.
    #
    #                                          DOCKER SERVICE HANDLERS / alias
    #TODO: ensure docker-container exists;
    function build {
        $_d  build
    }

    function up {
        #environment: Start containers.
        $_d  up -d
    }

    function down {
        $_d  down
    }

    function restart {
        #environment: Restart containers.
        down && up
    }

    function access {
        $_d exec "$1" bash
    }

    #
    # Recipes
    function configure {
        #environment: Build containers and installs requirements.

        $_e "${Cb}Installing..${Cn}"
        success message "Done.\n\nUse: \`${Cb}$0${Cn} up\`     To start containers."

        [ ! -e docker-compose.yml ] \
            && fail "Please configure a docker-compose file before continue."

        build

        restart

        success

        fail "Not configured."
    }

    function destroy {
        #environment: Destroy containers and images.

        fail "Not configured."
    }

    function status {
        #environment: Check configurations and status.

        fail "Not configured."
    }

    function deploy {
        #environment: Execute deploy procedures.

        fail "Not configured."
    }

    checkOptions "$@"
}
