#!/usr/bin/env bash
#
# TITLE       : λ::lambda Recipes
# DESCRIPTION : Receitas de gerenciamento simples dos comandos de manutenção do projeto.
# AUTHOR      : John Murowaniecki <john@compilou.com.br>
# DATE        : 20181006
# VERSION     : 0.3
# USAGE       : make
# USAGE       : bash λ
# USAGE       : ./λ
# USAGE       : ./λ help
# ----------------------------------------------------------------------------
#

function gitignore {
    not=.gitignore
    not=$(cat $not)
    rules=('#.*/' '\\/\\\\' '\./\\\.' '\-/\\\-' '\*/\\\*' '\//\\\/')

    for filter in ${rules[@]}
    do not=$($_e "${not}" | sed -e "s/${filter}/g")
    done

    rules='^\.[a-zA-Z0-9]+' # deny invisibles
    for each in ${not[@]}
    do rules="${rules}|${each}"
    done

    $_e "${rules}"
}
