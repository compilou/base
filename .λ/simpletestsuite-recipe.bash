#!/usr/bin/env bash
#
# TITLE       : λ::lambda Recipes
# DESCRIPTION : Receitas de gerenciamento simples dos comandos de manutenção do projeto.
# AUTHOR      : John Murowaniecki <john@compilou.com.br>
# DATE        : 20181006
# VERSION     : 0.3
# USAGE       : make
# USAGE       : bash λ
# USAGE       : ./λ
# USAGE       : ./λ help
# ----------------------------------------------------------------------------
#

function steps {
    # Just a small test suite in bash
    header=
    target=
    error=0
    debug=

    function aim {
        header="${Cd}${1}${Cn}: "
        target="${2}"
        method="${3:-GET}"
    }

    function try {
        if [ ${#} -gt 2 ]
        then data="${3}"; send="-d ${2}"
        else data="${2}"; send=
        fi

        $_e -en  "   ${header}${1}\r"
        curl=$(curl "${target}"    "${send}" -X "${method}" -s)
        pipe=$($_e  "${curl}"|grep "${data}")

        [ "${pipe}"  == "" ] && $_e -n " ${Cr}✗" && let  error++
        [ "${pipe}"  != "" ] && $_e -n " ${Cg}✓";  $_e   "${Cn}"
        [ "${debug}" != "" ] && $_e "\tdebug: ${Cd}${curl}${Cn}\n"
    }

    [ "$1" == "debug" ] && debug=__ACTIVE__

    if [ "$debug" != "" ]
    then
        name=$2
        args=$3
    else
        name=$1
        args=$2
    fi

    function custom_help {
        $_e "Custom help ${Cb}done${Cn}"
    }

    function steps_config {
        [ "$1" == "" ] && custom_help && fail "No filename given."
        path=tests/simple
        file="${path}/$1.λ.steps"
    }

    function new {
        #steps: Create a new test file.
        steps_config "$@"  && success message "Created steps file ${Cb}${file}${Cn}."
        mkdir -p "${path}"
        touch    "${file}" && success
    }

    function run {
        #steps: Execute test files.
        steps_config "$@"  && success message "Running steps file ${Cb}${file}${Cn}."
        [ ! -e "${file}" ] && fail "${Cb}${file}${Cn} does not exist."
        success && eval "$(cat "${file}")" # Bash: the mother of every workaround (;
    }

    function list {
        #steps: List test files.

        pattern="*${1:-*}*.λ.steps"
        $_e "Listing step files: ${pattern}"
        steps_config      "${pattern}" && success message "\nUsing pattern ${path}/${Cb}${pattern}${Cn} done."
        for   e in ${path}/${pattern}
        do [ -e  "${e}" ] \
             &&   $_e "-${Cb}\t$(
             for n in $($_e "${e}"|tr '/' ' ')
             do e=;done;$_e "$n"  |tr '.' ' ' | awk '{print($1)}')${Cn}"
        done && success
    }

    function steps_install {
        aim 'Checking installation' \
            'http://localhost/'
            try 'Static container' 'cms'
    }

    checkOptions "$@"
}
