#!/usr/bin/env bash
#
# TITLE       : λ::lambda Recipes - Validação.
# DESCRIPTION : Receitas de gerenciamento simples dos comandos de manutenção do projeto.
# AUTHOR      : John Murowaniecki <john@compilou.com.br>
# DATE        : 20181006
# VERSION     : 0.3
# USAGE       : make
# USAGE       : bash λ
# USAGE       : ./λ
# USAGE       : ./λ help
# ----------------------------------------------------------------------------
#

#
# TODO:
#   - ensure linter for some most common languages;
#
function check {
    # Source linter and validations

    LINTER="php -l" && aOK='No syntax errors detected in'
    [ -e "php vendor/bin/phpcs" ] && LINTER='php vendor/bin/phpcs' && aOK=''

    function all {
        #check: Check every structure

        execute "./"
    }

    function [folder_name] {
        #check: Check another desired folder.

        execute "$@"
    }

    function execute {
        [ ${#} -lt 1 ] && fail

        folders=${folders:-"${*}"}
        ignored=$(gitignore)

        for local in ${folders}
        do  r=${Cg}
            [ ! -d "${local}" ] && break

            failures=0
            files=$(find "${local}" -name "*.php" )
            for d in ${files}
            do
                d=$($_e "${d}" | sed 's/\/\//\//g')

                if [ ! -z "$($_e "${d}" | grep -E "${ignored}")" ]; then
                    continue
                fi
                if [ -e "${d}" ]; then
                    $_e -n "   Validating file ${Cb}${d}${Cn}.."

                    tmp=$(${LINTER} "${d}")
                    nOK=$($_e "${tmp}" | grep "${aOK}")

                    if [ "${tmp}" == "${aOK}" ] || [ ! -z "${nOK}" ]
                    then $_e "\r ${Cg}✓${Cn}"
                    else $_e "\r ${Cr}✗${Cn}"
                        r=${Cr}
                        R="${R}${Cb}${d}${Cn}: ${tmp}\n\n"
                        let 'failures+=1'
                    fi
                fi
            done
            [ ${failures} -lt 1 ] && failures='no'
            $_e "\nThere are ${r}${failures}${Cn} failures on ${Cb}${local}${Cn} using \`${Cb}${LINTER}${Cn}\`."
        done
        [ "${R}" != "" ] && $_e "\nResume:\n${R}"

        success
    }

    [ -d "$1" ] && '[folder_name]' "$@" && exit

    checkOptions "$@"
}
